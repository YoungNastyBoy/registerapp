NOTE: I am still currently working on this project and it will be updated regularly

This is my first pseudo-commercial project as well as the first application I'm designing from scratch by myself. The purpose of the application
is to store workers, divided into subgroups admin and user (lack of better name), clients, and data of projects in database.
All admins, as well as users, who created a given project data row, can grant access to that data to a given client. It will be displayed on 
demand from client panel. Clients, who passed their email into the database, will also recieve an email notification saying that they have been
granted visibility to a given data row. I will probably enhance the GUI a little, but since the most important part is the functionality, I am not 
really concerned about making it look fancy. 
